/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba1092473_1;

/**
 *
 * @author SergioIván
 */
public class Jugador extends Persona{
    
    public byte numero;
    public String categoria;
    public short goles;
    public long minutosjugados;
    public int salario;

    public Jugador(String nombre)
    {
        super(nombre);
    }
    
    public Jugador(String nombre, byte edad)
    {
        super(nombre, edad);
    }
    public void anotaGol()
    {
        goles++;
    }
    public void cobrarSalario()
    {
        salario=+700000;
    }
   
    
    //[]Corchetes
    //{}Llaves
    //()Parentesis

    @Override
    public String toString() {
        
        return nombre+" num: "+numero;
                
                }
    
}
