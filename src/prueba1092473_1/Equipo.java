/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba1092473_1;

/**
 *
 * @author SergioIván
 */
public class Equipo {
    
    public String nombreequipo;
    public Tecnico entrenador;
    public Jugador[] jugadores;

    public Equipo(String nombreequipo) //Constructor e1
    {
        this.nombreequipo = nombreequipo;
    }

    public Equipo()//Constructor para e2
    {
    }

    public Equipo(String nombreequipo, Tecnico entrenador) //e3
    {
        this.nombreequipo = nombreequipo;
        this.entrenador = entrenador;
    }

    public Equipo(String nombreequipo, Tecnico entrenador, Jugador[] jugadores)//e4
    {
        this.nombreequipo = nombreequipo;
        this.entrenador = entrenador;
        this.jugadores = jugadores;
    }
      
        public void jugarPartido()
    {
    }

    @Override
    //Modificador   
    public String toString() {
        String equipoCOmpleto="";
        for (int i = 0; i < jugadores.length; i++) {
            //Jugador jugadore = jugadores[i];
            equipoCOmpleto=equipoCOmpleto+" "+jugadores[i].nombre+" "+jugadores[i].numero;
        }
 
        return "Nombre equipo: "+nombreequipo+"\nNombre Tecnico: "+entrenador+"\nJugadores: "+equipoCOmpleto;
    }
        
    
    
    
    
}
