/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba1092473_1;

/**
 *
 * @author SergioIván
 */
public class ClasePrincipal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Creacion del Objeto de tipo equipo
        Equipo e1 = new Equipo("Millonarios");//Metodo constructor creado
        Equipo e2 =new Equipo();//Metodo constructor por defecto
        //Creacion del objeto entrenador, que es de tipo Tecnico, y se usa como 
        //atributo del Objeto equipo        
        e1.entrenador=new Tecnico((byte)15,"Primertecnico",(byte)65 );
        e1.entrenador.nombre="Sergio";
        e1.entrenador.edad=(byte)56;
        e1.entrenador.id="39876542";
       
                     
        //Creaci[on del arreglo jugadores, que es un atributo de la clase equipo.
        e1.jugadores=new Jugador[5];
        
        //Creacion de los objetos jugador que estan en dentro del arreglo
        //Jugadores.
        for (int i = 0; i < e1.jugadores.length; i++) {
            e1.jugadores[i]=new Jugador("nombre");
            //e2.jugadores[i]=new Jugador("nombre");
        }
       
        //Se ingresa la informacion del jugador estrella.
        e1.jugadores[0].nombre="Sergio";
        e1.jugadores[0].edad=(byte)26;
        e1.jugadores[0].id="1092473";
        e1.jugadores[0].categoria="Profesional";
        e1.jugadores[0].goles=550;
        e1.jugadores[0].minutosjugados=1200;
        e1.jugadores[0].numero=(byte)10;
        e1.jugadores[0].salario=10000000; //DOLARES
        
        
        //Constructor e2
        
        e2.nombreequipo="Tolimita";
        e2.entrenador= new Tecnico((byte)15,"Entrenador",(byte)45);
        
        
        // TODO code application logic here
        
        
        //Constructor para e3
        Equipo e3 = new Equipo("Cali",new Tecnico((byte)10,"Otroentrenador",(byte)50));
        
        System.out.println(e1.nombreequipo);
        System.out.println(e1);
        
       // System.out.println(e2);
        
        
    }
    
}
